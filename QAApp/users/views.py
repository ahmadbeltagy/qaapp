from django.shortcuts import redirect, render
from django.views.generic import View
from django.http import HttpResponse
from django.contrib import messages
from .forms import UserRegistrationForm

# Create your views here.

class UserRegistration(View):
    def get(self,request):
       form = UserRegistrationForm()
       return render(request,"users/user-registration.html",{"form":form})

    def post(self,request):
        submitted_form = UserRegistrationForm(request.POST)
        if submitted_form.is_valid():
            messages.success(request,"Account created successfully")
            submitted_form.save()
        return render(request,"users/user-registration.html",{'form': submitted_form})
