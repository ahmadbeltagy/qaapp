from django.shortcuts import redirect, render
from django.views.generic import View
from .service import get_all_quizes,get_all_questions,get_quiz_score
from.service import submit_score
# Create your views here.

class DashBoardView(View):
    def get(self,request):
        context = {
            "quizes": get_all_quizes()
        }
        return render(request,"game/index.html",context)

class QuizView(View):
    def get(self,request,slug):
        context = {
        "questions":get_all_questions(slug),
        "slug":slug
        }
        return render(request,"game/quiz.html",context)
    
    def post(self,request,slug):
        submitted_values = request.POST.dict()
        submitted_values.pop('csrfmiddlewaretoken')
        quiz_score = get_quiz_score(submitted_values)
        submit_score(request.user,quiz_score,slug)
        context = {
            "quiz_name":slug ,
            "score": quiz_score,
            "max_score": len(submitted_values)
        }
        return render(request,'game/quiz-score.html',context)
