from django.db import models
from django.utils.text import slugify
import string 
import random 
  
def random_string_generator(size = 10, chars = string.ascii_lowercase + string.digits): 
    return ''.join(random.choice(chars) for _ in range(size)) 

# Create your models here.

class Exam(models.Model):
    title = models.CharField(max_length=100)
    type = models.CharField(max_length=50)
    slug = models.SlugField(unique=True,null=False,default="",editable=False)

    def save(self,*args,**kwargs):
        self.get_unique_slug()
        super().save(*args,**kwargs)

    def __str__(self) -> str:
        return f"Exam: {self.slug}"

    def slug_logic(self, i):
        i += 1
        slug_filter = Exam.objects.filter(slug__exact=self.slug)
        if len(slug_filter) == 1:
            if not slug_filter[0].pk == self.pk:
                self.slug = f"{slugify(self.title)}-{i}"
                self.slug_logic(i)

    def get_unique_slug(self):
        self.slug = slugify(self.title)
        i = 1
        self.slug_logic(i)



class Question(models.Model):
    description = models.CharField(max_length=200)
    image = models.ImageField(blank = True, null = True)
    exam = models.ForeignKey(Exam,on_delete=models.CASCADE, related_name="questions")

    def __str__(self) -> str:
        return f"Question: {self.description}"
    

class Answer(models.Model):
    text = models.CharField(max_length=100)
    is_correct = models.BooleanField()
    question = models.ForeignKey(Question,on_delete=models.CASCADE, related_name="answers")

    def __str__(self) -> str:
        return f"Answer: {self.text} for question id {self.question.id}"


