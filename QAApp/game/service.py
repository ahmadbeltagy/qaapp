from .models import Exam,Question,Answer
from leaderboard.models import LeaderBoard
def get_all_quizes():
    return Exam.objects.all()

def get_all_questions(slug):
    return Question.objects.filter(exam__slug=slug).prefetch_related('answers')

def get_correct_answers(keys):
    return Answer.objects.filter(question__id__in = keys, is_correct = True)

def get_quiz_score(submitted_answers: dict):
    score = 0
    keys = submitted_answers.keys()
    correct_answers = get_correct_answers(keys)
    
    for correct_answer in correct_answers:
        if str(correct_answer.text) == submitted_answers[str(correct_answer.question.id)]:
           score = score + 1 
    return score

def submit_score(user,score,slug):
    quiz = Exam.objects.get(slug = slug)
    user_score = LeaderBoard(username=user.username, user_id = user.id,score= score,quiz= quiz)
    user_score.save()



