from django.urls import path
from .views import DashBoardView, QuizView
from django.contrib.auth.decorators import login_required, permission_required

urlpatterns = [
    path('',login_required(DashBoardView.as_view()), name="dashboard"),
    path('quiz/<slug:slug>',login_required(QuizView.as_view()), name="quizStart")
]
