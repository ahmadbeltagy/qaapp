from django.shortcuts import render
from django.views.generic.base import View
from .service import get_all_quizes,get_quiz_leaderboard
# Create your views here.

class LeaderBoardIndexView(View):
    def get(self,request):
        quizes = get_all_quizes()
        context = {"quizes": quizes}
        return render(request,"leaderboard/index.html",context)

class GameLeaderBoardView(View):
    def get(self,request,slug):
        leaderboard_list,title  = get_quiz_leaderboard(slug)
        context = {"leaderboard_list": leaderboard_list, "title":title}
        return render(request,"leaderboard/leaderboard-table.html",context)