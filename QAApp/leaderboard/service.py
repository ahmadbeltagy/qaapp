from game.models import Exam
from .models import LeaderBoard

def get_all_quizes():
    return Exam.objects.all()

def get_quiz_leaderboard(slug):
    quiz = Exam.objects.get(slug = slug)
    return LeaderBoard.objects.filter(quiz = quiz.id).order_by('-score'), quiz.title