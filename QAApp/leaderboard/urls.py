from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import LeaderBoardIndexView,GameLeaderBoardView

urlpatterns = [
    path('',login_required(LeaderBoardIndexView.as_view()), name="leaderboard-index"),
    path('<slug:slug>',login_required(GameLeaderBoardView.as_view()), name="leaderboard"),
    
]
