from django.db import models
from game.models import Exam
# Create your models here.

class LeaderBoard(models.Model):
    username = models.CharField(max_length=50)
    user_id = models.IntegerField()
    score = models.IntegerField()
    created_at = models.DateField(auto_now=True)
    quiz = models.ForeignKey(Exam,related_name="leaderboard", on_delete=models.CASCADE, null=True)

    def __str__(self) -> str:
        return f"Leaderboard for quiz {self.quiz.title}"