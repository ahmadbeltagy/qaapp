# django-app
This is a django Q &amp; A app built for learning purposes
where I explored:
1. Django Template language
1. Forms handling
1. Views (function based and class based)
1. Data and Models
1. Admin dashboard
1. File uploads
1. Serving Static files
1. Sessions
1. Django ORM queries
1. Authentication and authorization middlewares
1. deployment
